from scipy import misc
from sys import argv
import numpy as np

"""
if len(argv)!=2:
	print("Cal posar el nom de la imatge a processar")
	quit()
"""

def imprimirIMG(img):
	(fila,col,prof)=img.shape
	for i in range(fila):
		for j in range(col):
			print "[%3d,%3d,%3d] "%(img[i][j][0],img[i][j][1],img[i][j][2]),

		print("")


g_image = misc.imread('Eye.png')
print type(g_image)
(g_rows,g_cols,colors)=g_image.shape
print ("la imatge la %d-%d amb una profunditat de %d"%(g_rows,g_cols,colors))

size=8  # or 8 or 16
p1=g_image[1][0]
print(p1)


l_rows=int(g_rows/size)
l_cols=g_cols

print("Dividint una imatge de %dx%d entre %d es queda %dx%d"%(g_rows,g_cols,size,l_rows,l_cols))

imprimirIMG(g_image)

l_img=np.empty((l_rows,l_cols),dtype='uint8')
imprimirIMG(l_img)
