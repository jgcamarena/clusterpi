# -*- coding: utf-8 -*-

"""
Programa per veure el funcionament de l'enviament
Enviarà un vector a tots els processos. Cada process mostrara
La celda que lo correspon
"""
import numpy
import time
import sys

def sumar(v,inici,fi):
	sum=0
	for i in range(inici,fi):
		sum+=int(v[i])
	return sum

tamany=int(sys.argv[1])

suma=0
start=time.time()
data = numpy.arange(tamany, dtype='i')
suma=sumar(data,0,tamany)
end=time.time()
print("La suma total es %d, calculat en %3.4f"%(suma,end-start))




