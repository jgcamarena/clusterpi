---
title: Computació paral·lela amb cluster de Raspberry Pi
description: Taller de computació Paral·lela. Farem servir un cluster de raspberry Pi, programanat amb MPI amb Pytohn
marp: true
theme: default 
paginate: true
# backgroundImage: url('https://marp.app/assets/hero-background.jpg')
_paginate: false
_header: ''
_footer: ''
style: |
  section.centrar h1{
    text-align: center;
    font-size: 70px;
    background-color: lavender;
  }
  header, footer{ 
    font-size: 90%;
    text-align: center; 
  }
  p{
    font-size: 100%
  }
  section.menor p, table, ul{
    font-size: 80%
  }

---
<!-- _class: centrar -->

# A programar...

---
# Primer programa en Python
Programa que mostra la taula de multiplicar del 4

```python
tam=10
numero=4
for i in range(10+1):
  print ("%2d x %2d = %2d"%(i,numero,i*numero))
```
---

# El hola món en mpi

```python
# -*- coding: utf-8 -*-
from mpi4py import MPI
import sys

size=MPI.COMM_WORLD.Get_size()
rank=MPI.COMM_WORLD.Get_rank()
name=MPI.Get_processor_name()

print("Hello world, Soc el procés %2d de %2d al node %10s" %(rank,size,name))
```

---
# El fitxer `machinefile`

Al fitxer `machinefile` indicarem els nodes que tenim al cluster, així com el numero de nuclis (cores), que disposa cada node. Per exemple:

```
# machinefile

node1
node2:4
node3:2
node4:4
```
Indiquem que:

- El node1 sols te 1 core
- El node2 i node4 tenen 4 cores
- El node3 te dos cores

---

# Execució
```
mpiexe -np num_processos -f machinefile python script arguments
```

on:
- S'executen `num_processos` processos, 
- el conjunt de màquines es descriu en l'arxiu `machinefile`
- `python script arguments` indica que carrega l'interpret de python executar el script amb uns arguments donats

---

# `-np num_processos`

A l'opció `-np num_processos` tenim la clau:
```
mpiexe -np 8 -f machinefile python script

size=MPI.COMM_WORLD.Get_size()
rank=MPI.COMM_WORLD.Get_rank()
name=MPI.Get_processor_name()
```

- `size`: en tots els processos valrdrà 8
- `rank`: en cada procés serà distint. Com hi han 8 processos, valdrà respectivament 0, 1, 2 ,... fins a 7.
- `name`: serà el nom de la màquina on s'executa. Si alguna màquina té més de un core, evidentment serà compartit

---

# Com es comporten els processos

---

![bg](https://miro.medium.com/max/724/1*G93vt6VRasohfOery4AKQQ.png)

---

![bg](https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/minions-1558527220.jpg?crop=1.00xw:0.691xh;0,0&resize=1200:*)

---

![bg right](https://i.blogs.es/d8638d/cartel-20gru/450_1000.jpg)

# Algú ha de posar ordre

Qui? 

El master, o qui coordinarà les tasques a fer. Habitualment serà aquell que `rank==0`

---

# Com es fa un programa paral·le

```python
posem coses en comú

if rank==0:
    repartir les tasques

rebre la tasca de root

fer les tasques

enviar resultats a root

if rank==0:
    resumir/presentar els resultats
```

---

# Com es comuniquem root i la resta de processos

![](./mpi4py-1.jpg)

Amb l'objecte `comm` vist anteriorment, mitjançant enviament de missatges (`mpi`: _message pass interface_)

---

# Exemple bàsic (1)

Volem fer un programa que li donem a cada node un número. Cada procés el multiplicara pel seu `rank` i li'l tornarà al master.
```python
#                       Codi de el master
if rank==0:
  for i in range(1,size):
    data=random.randint(1, 10)
    comm.send(data,dest=i,tag=1)
  
  acabats=0
  while acabats<size:
    resposta=comm.recv(source=MPI.ANY_SOURCE,tag=1)
    proc=comm.recv(source=MPI.ANY_SOURCE,tag=2)
    print("El proces %d m'ha tornat un numero %d"%(proc,resposta))
```

---

```python
#                       Codi dels nodes
else:
  my_data=comm.recv(source=0,tag=MPI.ANY_TAG,status=status)
  tag=status.Get_tag()
  if tag==1: # datos
    x=my_data*rank
    comm.send(x,dest=0,tag=1)
    comm.send(rank,dest=0,tag=2)
```
![](https://t4.ftcdn.net/jpg/02/85/04/45/360_F_285044595_YohrOsRBOT72ot8iseEV5RxoTbrM3Q5R.jpg)

---
# Exemple més avançat. Suma paral·lela d'un vector

Tots sabem com es suma un vector. Amb un bucle for, des del primer fins a l'últim anem sumant i acumulant. 

Imaginem que tenim un vector de 32000 enters. Com podriem fer paral·lel l'algorisme de sumar un vector, si tenim per exemple 8 o 16 programes en execució?

> **Resposta**: Dividim el vector segons el numero de processos, i cada proces suma la seua part. Cada procés retorna la seua part de la suma i el node master l'arreplega de nou.

---

![](./Suma_Vector.png)

---
# Suma de vectors (1)

Calculem els tamnys. El `root` crea el vector (1,2,3....) i es fa un broadcast del vector per a que tots el tinguen
```python
tamany=int(sys.argv[1])
bloc=tamany/size

start=time.time()

if rank == 0:
   data =numpy.arange(tamany,dtype='i')
else:
   data = None

data=comm.bcast(data,root=0)
```
---
# Suma de vectors (2)

Llavors deprès, TOTS el processos sumen la seua porció del vector

```python
inici=rank*bloc
final=(rank+1)*bloc
suma=0
for i in range(inici,final):
	suma+=int(data[i])
```

i retornarem les dades al `root`

---

# Suma de vectors (3)
Fixar-se que:
 - `root` rebrà les dades dels nodes i acumularà
 - els nodes sols enviaràn la seua part de la suma ``
```python
if rank==0:
	total=suma #la meua suma
	for i in range(size-1):
		parcial=comm.recv(source=MPI.ANY_SOURCE,tag=1)
		total+=parcial
	
  end=time.time()
	print("La suma del vector es %d calculat en %2.5f"%(total,end-start))
else:
	comm.send(suma,dest=0,tag=1)
```
---

# Suma de vectors (i4)

Aquesta solució no és del tot bona. Perquè cres això?

![bg right](https://www.ing.uc.cl/estructural-y-geotecnica/wp-content/uploads/2019/07/question-mark-2405202_960_720-e1562008267872.png)


---

# Comunicacions avançades

## Broadcast (vist a l'exemple anterior)

![](./broadcasting.jpg)

---
# Broadcast 
```python
```python
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

# El que envia té la informació
if rank == 0:
   data = 34;   #Pot-ser qualssevol tipus i valor
   # data={"nom":"Pepe","edat":43}
   # data= [3,4,5,6,7,8]
else:       # El que rep te sols la variable
    data = None

# -- Difussió --
data = comm.bcast(data, root=0)

# -- En totes el processos el mateix valor
print('Al node ' + str(rank): )
print(data)
```
---
# Scatter (1)

![](./scatter.jpg)

Es tracta de dividir i repartir en parts iguals la tasca a processar

---
# Scatter (2)
```python
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
    # crea el vector [1,4,9,16]
   data = [(i+1)**2 for i in range(size)]
else:
   data = None

data = comm.scatter(data, root=0)
print ("Soc el proces %d i he rebut: "%rank)
print (data)
```
---

# Scatter (3)

Observar que:
 
- La data que dividim és la entrada de la funció (està com a argument) i també com a eixida (a l'esquerre de l'igual). 
- Així aconseguim que el node master dispose també de la seua porció de dades
- el segon argument (`root=0`) indica que és el procés amb `rank==0` qui farà l'enviament d'informació, mentre que els altres sols rebran la informació.

---

# Scatter (4)

Cas que l'objecte a transferir sigui algun objecte més complex o més gran, com un array de la llibreria `numpy` és convenient fer servir altra funció per a repartir. 

Aquesta nova manera implica fer una reserva de memòria prèvia per al vector que es rebrà al destí. Vegem l'exemple

---

# Scatter (5)

```python
tamany=1000
bloc=tamany/size

# Al master creem un vector amb numpy. arrange el crea amb la serie 1,2,3,4,5...
if rank == 0:
   g_data =numpy.arange(tamany,dtype='i')
else:
    # als altres es cal la referència, encara que no continga res
   g_data = None

# Ara preparem el vector que contindrà la porció del gran.

# El creem buit mitjançant empty. 
l_data=numpy.empty(bloc,dtype='i')

# Fem la divisió

# dividim lo global a lo local
comm.Scatterv(g_data,l_data,root=0)

# ara tots els nodes processarien la seua part del vector, root també
```

---

# Gather (1)

Com podreu deduir, si hem pogut fragmentar un vector en diversos més menuts, deuirem de poder reajuntar dits trossos més menuts en un global.

![](./gather.jpg)

---

# Gather (2)

Per a fer-ho, un cop processats les dades o vectors el més important serà:

- reservar la memòria al root per a rebre tota la informació
- Decidir com voldrem estructurar dita informació, ja que depenent de com creem la estrcutura, s'ajuntarà d'una manera o d'altra

---

# Gather - Escalar a vector

En aquest cas partim de que cada node (per exemple 4) te un escalar (un valor simple). Al reunir-lo el root tindrà un vector de 4 elements 

![](./gather_01.png)
```python
# En tots els nodes, creem un valor distint en cadascun 1,4,9,16,..
data = (rank+1)**2

# Enviem tots a root les nostres dades simples
data = comm.gather(data, root=0)

if rank == 0:
	print "Les dades que he rebut son ", data
```
---

# Gather -  Vector a vector

Si en cada node tenim un vector (per exemple 4 nodes en vectors de 3 elements). Ho podem juntar tot en un gran vector (4x3=12 elements)

![width:550](./gather_02.png)

```python
sendbuf = np.zeros(tam, dtype='i') + rank  # Un array de tam a valors el rank del node

recvbuf = None

# Sols el node master fa reserva de memòria
if rank == 0:
	recvbuf = np.empty([size * tam], dtype='i')
```

---
# Gather - Vector a matriu

```python
sendbuf = np.zeros(tam, dtype='i') + rank  # Un array de tam a valors el rank del node

recvbuf = None

# Sols el node master fa reserva de memòria
if rank == 0:
	recvbuf = np.empty([size, tam], dtype='i')
```

## Que ha canviat??

---

# Gather - Juguem amb els dimensions

Per acabar, al reunir la informació podem jugar conforme vullguem, sempre i quant s'acomplisca la següent norma: **la quantitat de dades que s'envie serà la quantitat de dades que es coloquen**.

Si enviem 4 vectors de 3 elements (12), podem formar una matriu de 2x6 (12)


![](./gather_03.png)

---

# Gather - Jugant amb les dimensions

Si cada node (4) te una matriu  de 3x3 (en total hi han 4x3x3=36 dades), com podem recomposar la informació?

![width:600](./gather_04.png)

---

# Gather - Jugant amb les dimensions

Con vullguem sempre que el producte de les dimensions del resultat sigui 36, inclòs una matriu de 3x3x4 o de 4x3x3, etc

![](./gather_05.png)

---

# Exemple. Procés d'una imatge en color

![width:800](imatge_color_bn.png)