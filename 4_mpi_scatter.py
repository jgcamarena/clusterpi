# -*- coding: utf-8 -*-

from mpi4py import MPI
import time

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
   data = [(i+1)**2 for i in range(size)]
else:
   data = None

data = comm.scatter(data, root=0)

time.sleep(rank/500)
print ("Jo soc el proc�s %d i les meues dades son"%rank)
print (data)
